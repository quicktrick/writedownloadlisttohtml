﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace WriteDownloadListToHTML
{
    class Program
    {
        static string urlTemplate = @"http://gallica.bnf.fr/iiif/ark:/12148/bpt6k12005345/f{0}/0,0,4000,8000/5000/0/native.jpg";
        static int startPage = 1;
        static int endPage = 882;

        // Set this to false to get the full hyperlink list
        static bool multiPageMode = true;
        static int[] multiplePages = { 0173, 0189, 0355 };

        static string localDir = @"d:\Languages\Dictionaries\DATA\French\Gallica\";
        static string fileName = "volume20.html";
        static string anchorTemplate = "page{0:0000}";

        static void Main(string[] args)
        {
            List<int> pageNumbers = new List<int>();

            if (!multiPageMode)
            {
                for (int i = startPage; i <= endPage; i++)
                {
                    pageNumbers.Add(i);
                }
            }
            else
                pageNumbers.AddRange(multiplePages);

            WriteHTML(pageNumbers, Path.Combine(localDir, fileName));
        }

        private static void WriteHTML(List<int> pageNumbers, string filePath)
        {
            XDocument doc = new XDocument(
                new XElement("html",
                    new XElement("head", ""),
                    new XElement("body",
                        new XElement("p", GetElements(pageNumbers).Select(el => el))
                    )
                )
            );

            var settings = new System.Xml.XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.Indent = true;
            //settings.IndentChars = ("\t");
            using (var xw = System.Xml.XmlWriter.Create(filePath, settings))
                doc.Save(xw);

            //doc.Save(filePath);
            //doc.Save(filePath, SaveOptions.DisableFormatting);
        }

        static List<XElement> GetElements(List<int> pageNumbers)
        {
            List<XElement> elements = new List<XElement>(pageNumbers.Count * 2);
            foreach (int pageNumber in pageNumbers)
            {
                elements.Add(new XElement("a", new XAttribute("href", string.Format(urlTemplate, pageNumber)), string.Format(anchorTemplate, pageNumber)));
                elements.Add(new XElement("br"));
            }
            return elements;
        }
    }
}
